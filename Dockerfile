FROM arm32v7/node:8-alpine3.9

WORKDIR /app

ENV PATH /app/node_modules/.bin:$PATH

# copy from local to container 
COPY package.json /app/package.json

RUN npm install
RUN npm install react-scripts 

CMD ["npm", "start"]
