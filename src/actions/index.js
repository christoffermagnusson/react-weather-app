import axios from 'axios';


const API_KEY = 'aed14b4f013c767fcb488851a11cba7c';
const BASE_URL = 'http://api.openweathermap.org/data/2.5/forecast?q='
//const BASE_URL = 'http://api.openweathermap.org/data/2.5/weather?q='; // previously used http://samples.... but now changed to http://api... and it worked
const APP_ID = `&appid=${API_KEY}`;

export const SEARCH_PERFORMED = 'SEARCH_PERFORMED';

export function performSearch(term){
	//console.log(`Searching for : ${term}`);
	let queryUrl = `${BASE_URL}${term}${APP_ID}`;
	console.log(queryUrl);
	let request = axios.get(queryUrl); // fires of an action with a promise attached. ReduxPromise will then take care of the promise and unpack it as the object
									   // and attach it to a brand new action to be fired AGAIN
	return {
		type:SEARCH_PERFORMED,
		payload:request,
	}
}


export const TERM_UPDATED = 'TERM_UPDATED';

export function updateTerm(term){
	//console.log(`Current term : ${term}`);
	return {
		type: TERM_UPDATED,
		payload:term,
	}
}