import React, { Component } from 'react';
import { connect } from 'react-redux';

import WeatherDetail from './weather_detail.js';


export default class WeatherForecastTable extends Component {
	constructor(props){
		super(props);
	}

	render(){
		return(
			<div className='table-responsive'>
				<table className='table'>
					<thead>
						<tr>
							<th>City</th>
							<th>Temperature</th>
							<th>Pressure</th>
							<th>Humidity</th>
						</tr>
					</thead>
					<WeatherDetail />
				</table>
			</div>
			)
	}
}






