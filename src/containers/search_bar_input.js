import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux'

import { updateTerm } from '../actions/index.js';

class SearchBarInput extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return (
			<input
			className='form-control'
			value={this.props.currentTerm}
			onChange={event => this.onInputChanged(event.target.value)}
			 />
			);
	}

	onInputChanged(term){
		this.props.updateTerm(term);
	}
}

function mapDispatchToProps(dispatch){
		return bindActionCreators({updateTerm:updateTerm}, dispatch);
}

function mapStateToProps(state){
	return {
		currentTerm:state.currentTerm,
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchBarInput);