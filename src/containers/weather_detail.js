import React, { Component } from 'react';
import { connect } from 'react-redux';

var LineChart = require('react-chartjs').Line;

// chart types data
const TEMP_TYPE 	= { type:'TEMP_TYPE', color:'rgba(193, 66, 66, 0.6)' };
const PRESSURE_TYPE = { type:'PRESSURE_TYPE', color:'rgba(63, 191, 191, 0.6)' };
const HUMIDITY_TYPE = { type:'HUMIDITY_TYPE', color:'rgba(127, 191, 63, 0.6)' };

class WeatherDetail extends Component{
	constructor(props){
		super(props);

		this.renderChart 		  = this.renderChart.bind(this);
		this.renderWeatherDetails = this.renderWeatherDetails.bind(this);
		this.decideDataType 	  = this.decideDataType.bind(this);
		this.decideColor 		  = this.decideColor.bind(this);
	}


	decideDataType(type, validData){
		switch(type){
			case TEMP_TYPE:
				return validData.map(dp => dp.main.temp);
				break;
			case PRESSURE_TYPE:
				return validData.map(dp => dp.main.pressure);
				break;
			case HUMIDITY_TYPE:
				return validData.map(dp => dp.main.humidity);
				break;
		}
	}

	decideColor(type){

	}

	renderChart(dataPoints, type){
		// Extracts valid data by filtering out the weather from each
		// day at noon (12). Data originally formatted as per 3 hours. (too many datapoints for the chart)
		let validData = dataPoints.filter(dp => {
			let timestamp = dp.dt_txt.split(' ');
			return timestamp[1].startsWith('12');
		});

		let dataType = this.decideDataType(type, validData);
		let chartData = {
			labels : validData.map(dp => dp.dt_txt),
			datasets : [
				{
					label: 'DATA',
					fillColor:type.color,
					strokeColor:'rgba(220,220,220,1)',
					pointColor:'rgba(220,220,220,1)',
					pointStrokeColor:'#fff',
					pointHighlightFill:'#fff',
					pointHighlightStroke:'rgba(220,220,220,1)',
					data: dataType,
				}
			]
		};

		return (
				<LineChart data={chartData} width='350' height='250' />
			);
	}

	renderWeatherDetails(cityData){
		if(!cityData){
			console.log("Found no cityData");
			alert("Found no data!");
			return <tr key="no_data"></tr>
		}

		let cityName = cityData.city.name;

		return(
			<tr key={cityName}>
				<td>{cityName}</td>
				<td>{this.renderChart(cityData.list, TEMP_TYPE)}</td>
				<td>{this.renderChart(cityData.list, PRESSURE_TYPE)}</td>
				<td>{this.renderChart(cityData.list, HUMIDITY_TYPE)}</td>
			</tr>
		)
	}


	render(){
		if(this.props.selectedCity.length==0){
			console.log("payload non existing");
			return <tbody></tbody>;
		}

		return(
				<tbody>
					{this.props.selectedCity.map(this.renderWeatherDetails)}
				</tbody>
			);
	}
}


function mapStateToProps(state){
	return{
		selectedCity:state.selectedCity,
	}
}


export default connect(mapStateToProps)(WeatherDetail);
