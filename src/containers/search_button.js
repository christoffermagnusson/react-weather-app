import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { performSearch } from '../actions/index.js';

class SearchButton extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return(
			<span className='input-group-btn'>
				<button
				type='submit'
				className='btn btn-secondary'
				onClick={() => this.performSearch()}
				>
				Search
				</button>
			</span>
			)
	}

	performSearch(){
		console.log(`Searching for : ${this.props.currentTerm}`);
		this.props.performSearch(this.props.currentTerm);
	}
}


function mapStateToProps(state){
	return {
		currentTerm:state.currentTerm
	}
}

function mapDispatchToProps(dispatch){
	return bindActionCreators({performSearch:performSearch},dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(SearchButton);