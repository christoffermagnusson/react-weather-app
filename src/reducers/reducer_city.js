import { SEARCH_PERFORMED } from '../actions/index.js';



export default(state = [], action) => {
	console.log(JSON.stringify(action));
	switch(action.type){
		case SEARCH_PERFORMED:
			return state.concat([ action.payload.data ]); // concatenates the new array into the old (creating a new array in the process)
		default:
			return state;
	}

}