import { TERM_UPDATED } from '../actions/index.js';


export default (state = null, action) => {
	//console.log(`reducer payload: ${action.payload}`);
	switch(action.type){
		case TERM_UPDATED:
			return action.payload;
			break;
		default:
			return '';  // return empty string if init or after a search, (auto empties input field)
	}
}