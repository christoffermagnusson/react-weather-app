import { combineReducers } from 'redux';

import SelectedCity from './reducer_city';
import CurrentTerm from './reducer_term';

const rootReducer = combineReducers({
	selectedCity:SelectedCity,
	currentTerm:CurrentTerm
});


export default rootReducer;