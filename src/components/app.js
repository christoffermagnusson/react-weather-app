import React, { Component } from 'react';

import SearchBar from './search_bar';
import WeatherForecastTable from '../containers/weather_forecast_table';
import WeatherDetail from '../containers/weather_detail';


export default class App extends Component{
	constructor(props){
		super(props);
	}

	render(){
		return(
				<div>
					<SearchBar />
					<WeatherForecastTable />
				</div>
			);


	}
}


