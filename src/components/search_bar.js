import React, { Component } from 'react';

import SearchBarInput from '../containers/search_bar_input';
import SearchButton from '../containers/search_button';

// Should update the app state "term", nothing else

export default class SearchBar extends Component{
	constructor(props){
		super(props);
	}

	onFormSubmit(event){
		event.preventDefault(); // Prevents default 'form' action when either submit for by pressing enter or btn
		// ajax request can be made here, but I'm making it in SearchButton instead..
	}

	render(){
		return(
			<div className="container input-group-container">
				<form onSubmit={this.onFormSubmit} className='input-group'>
					<SearchBarInput />
					<SearchButton />
				</form>
			</div>
			)
	}

}